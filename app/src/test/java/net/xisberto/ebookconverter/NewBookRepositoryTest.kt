package net.xisberto.ebookconverter

import kotlinx.coroutines.runBlocking
import net.xisberto.ebookconverter.new_ebook.NewEbookRepository
import org.junit.Assert
import org.junit.Test

class NewBookRepositoryTest {
    private val repository = NewEbookRepository()

    @Test
    fun getTitleTest() = runBlocking {
        repository.loadContent(
            "https://thobias.org/doc/sosed.html",
            object : NewEbookRepository.ContentCallback {
                override fun onResult(content: String?) {
                    print(content)
                    Assert.assertEquals(
                        content,
                        "só sed"
                    )
                }
            })
    }

    @Test
    fun getTitleEmptyTest() = runBlocking {
        repository.loadContent("", object : NewEbookRepository.ContentCallback {
            override fun onResult(content: String?) {
                Assert.assertEquals(content, null)
            }
        })
    }

    @Test
    fun convertTest() = runBlocking {
        repository.loadContent(
            "https://developer.android.com/jetpack/guide#cache-data",
            object : NewEbookRepository.ContentCallback {
                override fun onResult(content: String?) {

                }
            }
        )
    }
}