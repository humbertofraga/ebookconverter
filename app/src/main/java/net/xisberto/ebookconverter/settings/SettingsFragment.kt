package net.xisberto.ebookconverter.settings

import android.os.Bundle
import androidx.preference.PreferenceFragmentCompat
import net.xisberto.ebookconverter.R

class SettingsFragment : PreferenceFragmentCompat() {

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.root_preferences, rootKey)
    }
}