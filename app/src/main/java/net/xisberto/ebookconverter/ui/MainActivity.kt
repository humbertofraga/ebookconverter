package net.xisberto.ebookconverter.ui

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.onNavDestinationSelected
import androidx.navigation.ui.setupActionBarWithNavController
import dagger.hilt.android.AndroidEntryPoint
import net.xisberto.ebookconverter.R
import net.xisberto.ebookconverter.new_ebook.NewEbookFragment

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    private lateinit var appbarConfiguration: AppBarConfiguration
    private var url: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        when (intent?.action) {
            Intent.ACTION_SEND -> {
                if ("text/plain" == intent.type) {
                    intent.getStringExtra(Intent.EXTRA_TEXT)?.let { value ->
                        url = value
                    }
                }
            }
        }

        val toolbar = findViewById<Toolbar>(R.id.toolBar)
        setSupportActionBar(toolbar)

        val host: NavHostFragment = supportFragmentManager
            .findFragmentById(R.id.nav_host_fragment) as NavHostFragment? ?: return
        val navController = host.navController
        // Pass EXTRA_TEXT argument to navController graph/fragment
        val args = Bundle()
        args.putString(NewEbookFragment.ARG_URL, url)
        host.arguments = args
        navController.setGraph(R.navigation.main, args)

        appbarConfiguration = AppBarConfiguration(navController.graph)
        setupActionBarWithNavController(navController, appbarConfiguration)

    }

    override fun onSupportNavigateUp(): Boolean {
        return findNavController(R.id.nav_host_fragment).navigateUp(appbarConfiguration)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val retValue = super.onCreateOptionsMenu(menu)
        val navController = findNavController(R.id.nav_host_fragment)
        if (navController.currentDestination?.id == R.id.settings_fragment_dest)
            return retValue
        menuInflater.inflate(R.menu.main, menu)
        return retValue
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val retValue = item.onNavDestinationSelected(findNavController(R.id.nav_host_fragment))
                || super.onOptionsItemSelected(item)
        invalidateOptionsMenu()
        return retValue
    }
}