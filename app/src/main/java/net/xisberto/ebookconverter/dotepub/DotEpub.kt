package net.xisberto.ebookconverter.dotepub

import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST
import retrofit2.http.Streaming

interface DotEpub {

    @POST("post")
    @FormUrlEncoded
    @Streaming
    suspend fun convert(
        @Field("html") html: String,
        @Field("title") title: String,
        @Field("url") url: String,
        @Field("format") format: String = "epub"
    ): Response<ResponseBody>;
}