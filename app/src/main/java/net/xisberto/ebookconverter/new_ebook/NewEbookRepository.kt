package net.xisberto.ebookconverter.new_ebook

import android.content.Context
import android.util.Log
import net.xisberto.ebookconverter.dotepub.DotEpub
import okhttp3.*
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.io.InputStream
import java.net.UnknownHostException

class NewEbookRepository() {
    private val okHttpClient = OkHttpClient()
    private val URL = "https://dotepub.com/v1/"

    suspend fun loadContent(url: String, callback: ContentCallback) {
        if (url.isEmpty()) {
            callback.onResult(null)
            return
        }

        val request: Request?
        try {
            request = Request.Builder()
                .url(url)
                .build()
        } catch (e: IllegalArgumentException) {
            return
        } catch (e1: UnknownHostException) {
            return
        }

        request?.let {
            okHttpClient.newCall(request).enqueue(object : Callback {
                override fun onFailure(call: Call, e: IOException) {
                    e.printStackTrace()
                    callback.onResult(null)
                }

                override fun onResponse(call: Call, response: okhttp3.Response) {
                    response.use {
                        if (!response.isSuccessful)
                            throw IOException("Unexpected error $response")

                        response.body()?.let {
                            callback.onResult(it.string())
                        }
                    }
                }

            })
        }
    }

    suspend fun convert(
        context: Context,
        html: String,
        title: String,
        url: String,
        format: String = "epub",
        callback: ConvertCallback
    ) {
        val webservice = Retrofit.Builder()
            .baseUrl(URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(DotEpub::class.java)
        val response = webservice.convert(html, title, url, format)
        val contentType = response.headers().get("content-type")
        if (contentType.equals("text/xml;charset=utf-8")) {
            callback.onError("Erro ao converter a página")
        } else {
            callback.onSuccess(saveFile(context, response))
        }

    }

    private fun saveFile(context: Context, response: Response<ResponseBody>): String {
        val contentDisposition = response.headers()["content-disposition"]
        val filename = contentDisposition?.let {
            return@let Regex("filename=\"(.*)\"")
                .find(it)?.groupValues?.get(1)
        }
        filename?.let {
            response.body()?.let { body ->
                var input: InputStream? = null
                try {
                    input = body.byteStream()
                    val file = File(context.cacheDir, filename)
                    val fos = FileOutputStream(file)
                    fos.use { output ->
                        val buffer = ByteArray(4 * 1024)
                        var read: Int
                        while (input.read(buffer).also { read = it } != -1) {
                            output.write(buffer, 0, read)
                        }
                        output.flush()
                    }
                    return file.absolutePath
                } catch (e: Exception) {
                    Log.e("NewEbookRepository", "saveFile:", e)
                } finally {
                    input?.close()
                }
            }
        }
        return ""
    }

    interface ContentCallback {
        fun onResult(content: String?)
    }

    interface ConvertCallback {
        fun onSuccess(filepath: String)
        fun onError(message: String)
    }
}