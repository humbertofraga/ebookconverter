package net.xisberto.ebookconverter.new_ebook

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import dagger.hilt.android.AndroidEntryPoint
import net.xisberto.ebookconverter.R
import net.xisberto.ebookconverter.databinding.NewEbookFragmentBinding

@AndroidEntryPoint
class NewEbookFragment : Fragment() {

    companion object {
        const val ARG_URL = "arg_url"

        fun newInstance(url: String?) = NewEbookFragment().apply {
            arguments = Bundle().apply {
                putString(ARG_URL, url)
            }
        }
    }

    private val viewModel: NewEbookViewModel by viewModels()
    private lateinit var binding: NewEbookFragmentBinding
    private var content: String = ""

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = NewEbookFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.textUri.editText?.doOnTextChanged { text, _, _, _ ->
            text?.let {
                if (it.length > 6) {
                    binding.textViewTitle.text = ""
                    viewModel.loadContent(it.toString())
                }
            }
        }

        binding.tgFormat.addOnButtonCheckedListener { _, checkedId, isChecked ->
            if (isChecked) {
                when (checkedId) {
                    R.id.btEpub -> viewModel.setFormat("epub")
                    R.id.btMobi -> viewModel.setFormat("mobi")
                }
            }
        }
        binding.button.setOnClickListener {
            context?.let {
                viewModel.convert(it)
            }
        }

        arguments?.getString(ARG_URL)?.let { url ->
            binding.textUri.editText?.setText(url)
        }

        viewModel.url.observe(viewLifecycleOwner, object : Observer<String> {
            override fun onChanged(t: String?) {
                binding.textUri.editText?.setText(t)
                viewModel.url.removeObserver(this)
            }
        })
        viewModel.title.observe(viewLifecycleOwner) {
            binding.textViewTitle.text = it
        }
        viewModel.body.observe(viewLifecycleOwner) {
            content = it
        }
        viewModel.ready.observe(viewLifecycleOwner) { ready ->
            binding.button.isEnabled = ready
        }
    }

}
