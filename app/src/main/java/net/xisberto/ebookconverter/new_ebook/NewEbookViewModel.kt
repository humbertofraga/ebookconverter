package net.xisberto.ebookconverter.new_ebook

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.util.Log
import android.widget.Toast
import androidx.core.content.FileProvider
import androidx.lifecycle.*
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import java.io.File
import javax.inject.Inject

@HiltViewModel
class NewEbookViewModel @Inject constructor(
    private val savedStateHandle: SavedStateHandle
) : ViewModel() {
    private val titleRepository = NewEbookRepository()
    private val _url = MutableLiveData<String>()
    val url: LiveData<String> = _url
    private val _title = MutableLiveData<String>()
    val title: LiveData<String> = _title
    private val _body = MutableLiveData<String>()
    val body: LiveData<String> = _body
    private val _format = MutableLiveData<String>("epub")
    private val format: LiveData<String> = _format
    private val _ready = MutableLiveData<Boolean>(false)
    val ready: LiveData<Boolean> = _ready

    fun setFormat(value: String) {
        _format.postValue(value)
    }

    private inline fun <T : Any> ifLet(vararg elements: T?, closure: (List<T>) -> Unit) {
        if (elements.all { it != null }) {
            closure(elements.filterNotNull())
        }
    }

    fun loadContent(url: String) {
        _ready.postValue(false)
        _url.postValue(url)
        viewModelScope.launch {
            titleRepository.loadContent(url, object : NewEbookRepository.ContentCallback {
                override fun onResult(content: String?) {
                    content?.let {
                        _title.postValue(
                            Regex("<(title|TITLE)>(.*)</(title|TITLE)>").find(content)
                                ?.groupValues?.get(2)
                        )
                        _body.postValue(it)
                        _ready.postValue(true)
                    }
                }
            })
        }
    }

    fun convert(context: Context) {
        ready.value?.let { isReady ->
            if (isReady) {
                ifLet(body.value, title.value, url.value) { (body_val, title_val, url_val) ->
                    viewModelScope.launch {
                        titleRepository.convert(
                            context,
                            body_val,
                            title_val,
                            url_val,
                            format.value ?: "epub",
                            object : NewEbookRepository.ConvertCallback {
                                override fun onSuccess(filepath: String) {
                                    Log.d("ViewModel", "onSuccess: ebook saved on $filepath")
                                    val fileUri: Uri? = try {
                                        FileProvider.getUriForFile(
                                            context,
                                            "net.xisberto.ebookconverter.fileprovider",
                                            File(filepath)
                                        )
                                    } catch (e: IllegalArgumentException) {
                                        Log.e("NewEbookViewModel", "onSuccess:", e)
                                        null
                                    }
                                    if (fileUri != null) {
                                        val emailIntent = Intent(Intent.ACTION_SEND)
                                        emailIntent.type = "vnd.android.cursor.dir/email"
                                        emailIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
                                        emailIntent.putExtra(Intent.EXTRA_STREAM, fileUri)
                                        emailIntent.putExtra(Intent.EXTRA_SUBJECT, filepath)
                                        context.startActivity(
                                            Intent.createChooser(
                                                emailIntent,
                                                "Send email..."
                                            )
                                        )
                                    }
                                }

                                override fun onError(message: String) {
                                    Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
                                }

                            })
                    }
                }
            }
        }
    }

}